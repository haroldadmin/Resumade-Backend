import Express from 'express';
import resumesRouter from './routes/resumes';
import validationRouter from './routes/validation'

const app = Express();

app.use(Express.json());
app.use("/api/generation", resumesRouter);
app.use("/api/validation", validationRouter);

const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});