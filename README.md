# Resumade Backend

This project aims to provide an API for the Resumade android application to interface with the resume-cli client.
For details on resume-cli, you should checkout [JSON Resume](https://github.com/jsonresume).