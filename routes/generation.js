import Router from 'express';
import schema from 'resume-schema';
import theme from 'jsonresume-theme-flat';

export const router = Router()

router.post("/", (req, res) => {
    const resumeJson = req.body;
    schema.validate(resumeJson, (err, report) => {
        if (err === null) return res.status(400).send(report);
        const html = theme.render(resumeJson);
        res.send(html);
    });
});