const router = require('express').Router();
const resumeSchema = require('resume-schema');

router.post("/", async (req, res) => {
    const json = req.body;
    resumeSchema.validate(json, (err, report) => {
        if (err === null) {
            return res.status(400).send({
                isValid: false,
                report: report
            });
        }
        res.send({
            isValid: true,
            report: null
        });
    });
});

module.exports = router;